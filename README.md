# alpine-udpxy

#### [alpine-x64-udpxy](https://hub.docker.com/r/forumi0721alpinex64/alpine-x64-udpxy/)
![Docker Image Version (tag latest semver)](https://img.shields.io/docker/v/forumi0721alpinex64/alpine-x64-udpxy/latest)
![Docker Image Size (tag)](https://img.shields.io/docker/image-size/forumi0721alpinex64/alpine-x64-udpxy/latest)
![MicroBadger Layers (tag)](https://img.shields.io/microbadger/layers/forumi0721alpinex64/alpine-x64-udpxy/latest)
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpinex64/alpine-x64-udpxy)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpinex64/alpine-x64-udpxy)
#### [alpine-aarch64-udpxy](https://hub.docker.com/r/forumi0721alpineaarch64/alpine-aarch64-udpxy/)
![Docker Image Version (tag latest semver)](https://img.shields.io/docker/v/forumi0721alpineaarch64/alpine-aarch64-udpxy/latest)
![Docker Image Size (tag)](https://img.shields.io/docker/image-size/forumi0721alpineaarch64/alpine-aarch64-udpxy/latest)
![MicroBadger Layers (tag)](https://img.shields.io/microbadger/layers/forumi0721alpineaarch64/alpine-aarch64-udpxy/latest)
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpineaarch64/alpine-aarch64-udpxy)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpineaarch64/alpine-aarch64-udpxy)
#### [alpine-armhf-udpxy](https://hub.docker.com/r/forumi0721alpinearmhf/alpine-armhf-udpxy/)
![Docker Image Version (tag latest semver)](https://img.shields.io/docker/v/forumi0721alpinearmhf/alpine-armhf-udpxy/latest)
![Docker Image Size (tag)](https://img.shields.io/docker/image-size/forumi0721alpinearmhf/alpine-armhf-udpxy/latest)
![MicroBadger Layers (tag)](https://img.shields.io/microbadger/layers/forumi0721alpinearmhf/alpine-armhf-udpxy/latest)
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721alpinearmhf/alpine-armhf-udpxy)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721alpinearmhf/alpine-armhf-udpxy)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64,aarch64,armhf
* Appplication : [udpxy](http://www.udpxy.com/)
    - udpxy is a UDP-to-HTTP multicast traffic relay daemon.



----------------------------------------
#### Run

```sh
docker run -d \
           -p 4000:4000/tcp \
           forumi0721alpine[ARCH]/alpine-[ARCH]-udpxy:latest
```



----------------------------------------
#### Usage

* URL : [http://localhost:40000/(rtp|udp)/(address)]



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| 4000/tcp           | Port to listen on                                |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |

